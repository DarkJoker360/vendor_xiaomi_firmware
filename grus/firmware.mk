# Firmware
PRODUCT_COPY_FILES += \
    vendor/xiaomi/firmware/grus/abl.elf:install/firmware-update/abl.elf \
    vendor/xiaomi/firmware/grus/aop.mbn:install/firmware-update/aop.mbn \
    vendor/xiaomi/firmware/grus/BTFM.bin:install/firmware-update/BTFM.bin \
    vendor/xiaomi/firmware/grus/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
    vendor/xiaomi/firmware/grus/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
    vendor/xiaomi/firmware/grus/devcfg.mbn:install/firmware-update/devcfg.mbn \
    vendor/xiaomi/firmware/grus/dspso.bin:install/firmware-update/dspso.bin \
    vendor/xiaomi/firmware/grus/dtbo.img:install/firmware-update/dtbo.img \
    vendor/xiaomi/firmware/grus/hyp.mbn:install/firmware-update/hyp.mbn \
    vendor/xiaomi/firmware/grus/keymaster64.mbn:install/firmware-update/keymaster64.mbn \
    vendor/xiaomi/firmware/grus/logfs_ufs_8mb.bin:install/firmware-update/logfs_ufs_8mb.bin \
    vendor/xiaomi/firmware/grus/logo.img:install/firmware-update/logo.img \
    vendor/xiaomi/firmware/grus/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
    vendor/xiaomi/firmware/grus/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
    vendor/xiaomi/firmware/grus/storsec.mbn:install/firmware-update/storsec.mbn \
    vendor/xiaomi/firmware/grus/tz.mbn:install/firmware-update/tz.mbn \
    vendor/xiaomi/firmware/grus/xbl.elf:install/firmware-update/xbl.elf \
    vendor/xiaomi/firmware/grus/xbl_config.elf:install/firmware-update/xbl_config.elf
